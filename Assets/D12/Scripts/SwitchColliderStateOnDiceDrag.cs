﻿using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(Collider))]
public class SwitchColliderStateOnDiceDrag : MonoBehaviour
{
    [SerializeField] private Collider _collider;

    private void Awake()
    {
        Assert.IsNotNull(_collider);
    }

    void Start()
    {
        DiceLogic.Instance.DiceStartDrag += OnDiceStartDrag;
        DiceLogic.Instance.DiceStopDrag += OnDiceStopDrag;

        OnDiceStopDrag();
    }

    private void OnDestroy()
    {
        DiceLogic.Instance.DiceStartDrag -= OnDiceStartDrag;
        DiceLogic.Instance.DiceStopDrag -= OnDiceStopDrag;
    }

    void OnDiceStartDrag()
    {
        _collider.enabled = true;
    }

    void OnDiceStopDrag()
    {
        _collider.enabled = false;
    }
    
    void Reset()
    {
        _collider = GetComponent<Collider>();
    }
}
