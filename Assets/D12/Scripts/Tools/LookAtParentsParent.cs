﻿using UnityEngine;

/** Use in editor to assign rotation of texts at dice sides */
public class LookAtParentsParent : MonoBehaviour
{
    #if UNITY_EDITOR
    void Reset()
    {
        var tgt = transform.parent.parent;
        transform.LookAt(tgt);
    }
    #endif

    #if !UNITY_EDITOR
    void Start()
    {
        Debug.LogWarning("This shouldn't be in build!");
    }
    #endif
}
