﻿using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

public class WatchDiceLogicComponent : MonoBehaviour
{
    [SerializeField] private TMP_Text _labelForDiceState = null;
    [SerializeField] private TMP_Text _labelForDiceResults = null;

    private int Accumulator
    {
        get => _accumulator;
        set
        {
            _accumulator = value;
            _labelForDiceResults.text = _accumulator.ToString();
        }
    }

    private int _accumulator = 0;

    void Awake()
    {
        Assert.IsNotNull(_labelForDiceState);
        Assert.IsNotNull(_labelForDiceResults);
    }

    void Start()
    {
        DiceLogic.Instance.DiceRollResult += OnDiceRollResult;
        DiceLogic.Instance.DiceStartsRoll += OnDiceStartRoll;
    }

    void OnDestroy()
    {
        DiceLogic.Instance.DiceRollResult -= OnDiceRollResult;
        DiceLogic.Instance.DiceStartsRoll -= OnDiceStartRoll;
    }

    void OnDiceRollResult(int result)
    {
        Accumulator += result;
        _labelForDiceState.text = result.ToString();
    }

    void OnDiceStartRoll()
    {
        _labelForDiceState.text = "?";
    }
}
