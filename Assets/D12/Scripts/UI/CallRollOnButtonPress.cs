﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class CallRollOnButtonPress : MonoBehaviour
{
    [SerializeField] private Button _button;

    private void Awake()
    {
        Assert.IsNotNull(_button);
    }

    private void Start()
    { 
        _button.onClick.AddListener(DiceLogic.Instance.WantRoll);
    }

    private void OnDestroy()
    {
        _button.onClick.RemoveListener(DiceLogic.Instance.WantRoll);
    }

    private void Reset()
    {
        _button = GetComponent<Button>();
    }
}
