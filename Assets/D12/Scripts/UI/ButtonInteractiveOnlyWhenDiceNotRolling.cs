﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonInteractiveOnlyWhenDiceNotRolling : MonoBehaviour
{
    [SerializeField] private Button _button;

    private void Awake()
    {
        Assert.IsNotNull(_button);
    }

    void Start()
    {
        DiceLogic.Instance.DiceStartsRoll += OnStartRoll;
        DiceLogic.Instance.DiceRollResult += OnStopRoll;
    }
    
    private void OnDestroy()
    {
        DiceLogic.Instance.DiceStartsRoll -= OnStartRoll;
        DiceLogic.Instance.DiceRollResult -= OnStopRoll;
    }

    void OnStartRoll()
    {
        _button.interactable = false;
    }

    void OnStopRoll(int _)
    {
        _button.interactable = true;
    }

    void Reset()
    {
        _button = GetComponent<Button>();
    }
}
