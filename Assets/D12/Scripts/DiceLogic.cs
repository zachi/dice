﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

/**
 * In perfect world this class wouldn't be MonoBehaviour, and would be injected by
 * Zenject/Extenject or other DI
 */
public class DiceLogic : MonoBehaviour
{
    public static DiceLogic Instance { get; private set; }
    public event Action<int> DiceRollResult;
    public event Action DiceStartsRoll;
    public event Action DiceStartDrag;
    public event Action DiceStopDrag;
    public event Action OnWantRoll;
    
    private readonly List<DiceComponent> _knownDices = new List<DiceComponent>();
    [SerializeField] private DiceConfig _config = null;
    public float RollThreshold => _config._rollThreshlod;

    void Awake()
    {
        Instance = Instance ?? this;
        Assert.IsNotNull(_config);
    }
    
    public void RegisterDice(DiceComponent diceComponent)
    {
        if (_knownDices.Contains(diceComponent))
            return;

        AttachDice(diceComponent);
    }

    public void UnregisterDice(DiceComponent diceComponent)
    {
        DetachDice(diceComponent);
        _knownDices.Remove(diceComponent);
    }

    public void WantRoll()
    {
        OnWantRoll?.Invoke();
    }

    void AttachDice(DiceComponent dice)
    {
        dice.StopRoll += WhenDiceStops;
        dice.StartRoll += WhenDiceStartsRoll;
        dice.StopDrag += DiceStopDrag;
        dice.StartDrag += DiceStartDrag;
    }

    void DetachDice(DiceComponent dice)
    {
        dice.StopRoll -= WhenDiceStops;
        dice.StartRoll += WhenDiceStartsRoll;
        dice.StopDrag -= DiceStopDrag;
        dice.StartDrag -= DiceStartDrag;
    }

    void WhenDiceStops(int result)
    {
        DiceRollResult?.Invoke(result);
    }

    void WhenDiceStartsRoll()
    {
        DiceStartsRoll?.Invoke();
    }
}
