﻿using TMPro;
using UnityEngine;

public class DiceSideMarker : MonoBehaviour
{
    public int ResultValue => _resultValue;
    public float Weight => transform.position.y;
    public bool IsValid => Vector3.Angle(_labelForValue.transform.forward, Vector3.up) < 1f;

    [SerializeField] private int _resultValue =  0;
    [SerializeField] private TMP_Text _labelForValue = null;

    void Start()
    {
        UpdateLabel();
    }

    public void UpdateValue(int val)
    {
        _resultValue = val;
        UpdateLabel();
    }

    void UpdateLabel()
    {
        if (_labelForValue)
            _labelForValue.text = FormattedDiceNumber(_resultValue);
    }

    string FormattedDiceNumber(int i)
    {
        if (i == 6 || i == 9)
            return $"{i}.";

        return i.ToString();
    }
}
