﻿#define EDIT_DICE_HELPER
using System;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

/**
 * Component attached to dice
 */
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(DiceInHand))]
public class DiceComponent : MonoBehaviour
{
    public event Action StartRoll;
    public event Action<int> StopRoll;
    public event Action StartDrag;
    public event Action StopDrag;

    [ContextMenuItem(nameof(Bump), nameof(Bump))]
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private DiceInHand _diceInHand;
    [SerializeField] private DiceSideMarker[] _sideMarkers;
    [SerializeField] private DiceConfig _config = null;
    [SerializeField] private bool _overrideValuesWithConfig = false;
    private bool _previousPhysicalFrameSleeping { get; set; }= false;
    private bool _rolling { get; set; } = false;
    
    void Awake()
    {
        Assert.IsNotNull(_rigidbody);
        Assert.IsNotNull(_config);

        if (_overrideValuesWithConfig)
        {
            for (int i = 0; i < _sideMarkers.Length; i++)
            {
                _sideMarkers[i].UpdateValue(_config._sides[i% _config._sides.Length]);
            }
        }
    }
    
    void Start()
    {
        DiceLogic.Instance.RegisterDice(this);
        DiceLogic.Instance.OnWantRoll += OnRequestRoll;
    }

    void OnRequestRoll()
    {
        if (_rolling)
            return;

        _diceInHand.enabled = false;
        StopDrag?.Invoke();
        var circRand = Random.insideUnitCircle.normalized;
        Vector3 randVector = new Vector3(
            circRand.x * 3f, 
            1f, 
            circRand.y * 3f);

        var velocity = randVector.normalized * (DiceLogic.Instance.RollThreshold * 2f);
        RollWithVelocity(velocity);
    }

    private void OnMouseDown()
    {
        if (_rolling)
            return;
        
        _diceInHand.enabled = true;
        StartDrag?.Invoke();
    }

    private void OnMouseUp()
    {
        if (_rolling)
            return;
        
        _diceInHand.enabled = false;
        StopDrag?.Invoke();
        if (_diceInHand.FrameVelocity.magnitude > DiceLogic.Instance.RollThreshold)
        {
            RollWithVelocity(_diceInHand.FrameVelocity);
        }
    }

    void RollWithVelocity(Vector3 velocity)
    {
        _rigidbody.velocity = velocity;
        _rigidbody.angularVelocity = new Vector3(
            Random.Range(-100, 100), 
            Random.Range(-100, 100), 
            Random.Range(-100, 100)
        );
        _rolling = true;
        StartRoll?.Invoke();
    }

    private void OnDestroy()
    {
        DiceLogic.Instance.OnWantRoll -= OnRequestRoll;
        DiceLogic.Instance.UnregisterDice(this);
    }

#if EDIT_DICE_HELPER && UNITY_EDITOR
    // helps set result markers
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            _rigidbody.isKinematic = !_rigidbody.isKinematic;
        }
    }
#endif

    private void FixedUpdate()
    {
        if (_rolling && _rigidbody.IsSleeping() && _previousPhysicalFrameSleeping == false && _rigidbody.isKinematic == false)
        {
            if (AnyValid())
            {
                StopRoll?.Invoke(DetermineResult());
                _rolling = false;
            }
            else
            {
                Bump();
            }
        }

        _previousPhysicalFrameSleeping = _rigidbody.IsSleeping();
    }

    bool AnyValid()
    {
        for (int i = 0; i < _sideMarkers.Length; i++)
        {
            if (_sideMarkers[i].IsValid)
                return true;
        }

        return false;
    }

    int DetermineResult()
    {
        var result = _sideMarkers[0];
        for (int i = 1; i < _sideMarkers.Length; i++)
        {
            var potentialResult = _sideMarkers[i];
            if (potentialResult.Weight > result.Weight)
                result = potentialResult;
        }
        
        return result.ResultValue;
    }

    void Bump()
    {
        _rigidbody.AddForce(Vector3.up * 50 );
        _rigidbody.angularVelocity = new Vector3(
            Random.Range(-100, 100), 
            Random.Range(-100, 100), 
            Random.Range(-100, 100)
            );
    }

    void Reset()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _diceInHand = GetComponent<DiceInHand>();
        _sideMarkers = GetComponentsInChildren<DiceSideMarker>();
        _diceInHand.enabled = false;
    }
}
