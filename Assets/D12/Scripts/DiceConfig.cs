﻿using UnityEngine;

[CreateAssetMenu(fileName="DiceConfig.asset", menuName = "DiceConig")]
public class DiceConfig : ScriptableObject
{
    public int[] _sides;
    
    [Tooltip("Minimal velocity to invoke roll")]
    public float _rollThreshlod;

    public Rect ClampXY = new Rect(-9, -9, 9, 9);
    public float RaycastDistance = 1000f;

}
