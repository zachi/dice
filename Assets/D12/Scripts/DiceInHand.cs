﻿using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(Rigidbody))]
public class DiceInHand : MonoBehaviour
{
    [SerializeField] private LayerMask _layerMask = 0;
    [SerializeField] private Rigidbody _rigidbody = null;
    [SerializeField] private DiceConfig _config = null;
    private Camera mainCamera { get; set; }

    private Vector3 _lastFramePosition;
    private Vector3 _frameVelocity;

    public Vector3 FrameVelocity => _frameVelocity;

    void Awake()
    {
        Assert.IsNotNull(_rigidbody);
        Assert.IsNotNull(_config);
        
        mainCamera = Camera.main;
    }
    
    void FixedUpdate()
    {
        var ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out var rHit, _config.RaycastDistance, _layerMask))
        {
            _lastFramePosition = _rigidbody.position;
            _rigidbody.position = SafeSpace(rHit.point);
            _frameVelocity = (rHit.point - _lastFramePosition) / Time.fixedDeltaTime;
        }
    }

    Vector3 SafeSpace(Vector3 rHit)
    {
        return new Vector3(
            Mathf.Clamp(rHit.x, _config.ClampXY.x, _config.ClampXY.width),
            rHit.y,
            Mathf.Clamp(rHit.z, _config.ClampXY.y, _config.ClampXY.height));
    }

    private void OnEnable()
    {
        _rigidbody.isKinematic = true;
    }

    private void OnDisable()
    {
        _rigidbody.isKinematic = false;
    }

    private void Reset()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
}
